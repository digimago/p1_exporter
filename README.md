# P1 exporter

[![GoDoc](https://img.shields.io/badge/godoc-reference-5272B4.svg)](https://godoc.org/gitlab.com/digimago/p1_exporter)

Prometheus exporter for DSMR (Dutch Smart Meter Requirements) using the end-consumer (P1) interface.
Forked from https://github.com/roaldnefs/p1_exporter

* [Installation](README.md#installation)
     * [Binaries](README.md#binaries)
     * [Via Go](README.md#via-go)
* [Usage](README.md#usage)

## Installation

### Binaries

### Via Go

```console
$ go get gitlab.com/digimago/p1_exporter
```

## Usage

```console
 $ p1_exporter--help
usage: p1_exporter --serial.port=SERIAL.PORT [<flags>]

Flags:
  -h, --help                     Show context-sensitive help (also try --help-long and --help-man).
      --web.listen-address=":9602"  
                                 Address on which to expose metrics and web interface.
      --web.telemetry-path="/metrics"  
                                 Path under which to expose metrics.
      --serial.port=SERIAL.PORT  Serial port for the connection to the P1 interface.
```
