package main

import (
	"bufio"
	"errors"
	"net/http"
	"strconv"

	"github.com/roaldnefs/go-dsmr"

	"github.com/tarm/serial"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gopkg.in/alecthomas/kingpin.v2"

	log "github.com/sirupsen/logrus"
)

var (
	powerMeterActual = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "p1",
		Subsystem: "electricity",
		Name:      "power_meter_kw",
		Help:      "Actual electricity power delivered (+P) and received (-P) in 1 Watt resolution.",
	}, []string{"direction"})
	powerMeterTotal = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "p1",
		Subsystem: "electricity",
		Name:      "power_meter_kwh_count",
		Help:      "Meter reading of electricity power delivered (+P), received (-P) in 1 KWh resolution.",
	}, []string{"tariff", "direction"})
	instVoltage = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "p1",
		Subsystem: "electricity",
		Name:      "instantaneous_voltage",
		Help:      "Instantaneous voltage in V per phase",
	}, []string{"phase"})
	instCurrent = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "p1",
		Subsystem: "electricity",
		Name:      "instantaneous_current",
		Help:      "Instantaneous current in A per phase",
	}, []string{"phase"})
	gasMeterTotal = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "p1",
		Subsystem: "gas",
		Name:      "gas_meter_m3_count",
		Help:      "Meter reading of gas meter in M3",
	})
	previousT2Delivered   float64
	previousT2Received    float64
	previousT1Delivered   float64
	previousT1Received    float64
	previousGasMeterTotal float64
	config                *serial.Config
)

func recordMetrics() {
	//	var channel = make(chan []byte)

	go func() {
		// Open serial port
		stream, err := serial.OpenPort(config)
		if err != nil {
			log.Fatal(err)
		}

		reader := bufio.NewReader(stream)

		for {
			// Peek at the next byte, and look for the start of the telegram
			if peek, err := reader.Peek(1); err == nil {
				// The telegram starts with a '/' character keep reading
				// bytes until the start of the telegram is found
				if string(peek) != "/" {
					_, err := reader.ReadByte()
					if err != nil {
						log.Warn(errors.New("could not read byte from reader"))
					}
					continue
				}
			} else {
				continue
			}

			// Keep reading until the '!' character which indicates the end of
			// the telegram and is followed by the CRC
			rawTelegram, err := reader.ReadBytes('!')
			if err != nil {
				log.Error(err)
				continue
			}

			// Read the CRC which can be used to detect faulty telegram
			// TODO check CRC
			_, err = reader.ReadBytes('\n')
			if err != nil {
				log.Error(err)
				continue
			}

			telegram, err := dsmr.ParseTelegram(string(rawTelegram))
			if err != nil {
				log.Error(err)
				continue
			}

			if err := updateMetricsFromTelegram(telegram); err != nil {
				log.Error(err)
				continue
			}

		}
	}()

}

func updateMetricsFromTelegram(telegram dsmr.Telegram) error {
	if rawValue, ok := telegram.InstantaneousVoltageL1(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		instVoltage.WithLabelValues("L1").Set(value)
	}

	if rawValue, ok := telegram.InstantaneousVoltageL2(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		instVoltage.WithLabelValues("L2").Set(value)
	}

	if rawValue, ok := telegram.InstantaneousVoltageL3(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			log.Error(err)
		}
		instVoltage.WithLabelValues("L3").Set(value)
	}

	if rawValue, ok := telegram.InstantaneousCurrentL1(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		instCurrent.WithLabelValues("L1").Set(value)
	}

	if rawValue, ok := telegram.InstantaneousCurrentL2(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		instCurrent.WithLabelValues("L2").Set(value)
	}

	if rawValue, ok := telegram.InstantaneousCurrentL3(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		instCurrent.WithLabelValues("L3").Set(value)
	}
	if rawValue, ok := telegram.ActualElectricityPowerDelivered(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		powerMeterActual.WithLabelValues("delivered").Set(value)
	}
	if rawValue, ok := telegram.ActualElectricityPowerReceived(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		powerMeterActual.WithLabelValues("received").Set(value)
	}
	if rawValue, ok := telegram.MeterReadingGasDeliveredToClient(1); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		var delta float64
		delta, previousGasMeterTotal = registerDelta(value, previousGasMeterTotal)
		gasMeterTotal.Add(delta)
	}
	if rawValue, ok := telegram.MeterReadingElectricityDeliveredToClientTariff1(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			return err
		}
		var delta float64
		delta, previousT1Delivered = registerDelta(value, previousT1Delivered)
		powerMeterTotal.WithLabelValues("t1", "delivered").Add(delta)
	}
	if rawValue, ok := telegram.MeterReadingElectricityDeliveredToClientTariff2(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			log.Error(err)
			return err
		}
		var delta float64
		delta, previousT2Delivered = registerDelta(value, previousT2Delivered)
		powerMeterTotal.WithLabelValues("t2", "delivered").Add(delta)
	}
	if rawValue, ok := telegram.MeterReadingElectricityDeliveredByClientTariff1(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			log.Error(err)
			return err
		}
		var delta float64
		delta, previousT1Received = registerDelta(value, previousT1Received)
		powerMeterTotal.WithLabelValues("t1", "received").Add(delta)
	}
	if rawValue, ok := telegram.MeterReadingElectricityDeliveredByClientTariff2(); ok {
		value, err := strconv.ParseFloat(rawValue, 64)
		if err != nil {
			log.Error(err)
			return err
		}
		var delta float64
		delta, previousT2Received = registerDelta(value, previousT2Received)
		powerMeterTotal.WithLabelValues("t2", "received").Add(delta)
	}
	return nil
}

func registerDelta(value float64, previousValue float64) (float64, float64) {
	var v float64
	if previousValue > 0 {
		v = value - previousValue
	} else {
		v = 0
	}
	return v, value
}

func main() {
	var (
		listenAddress = kingpin.Flag(
			"web.listen-address",
			"Address on which to expose metrics and web interface.",
		).Default(":9602").String()
		metricsPath = kingpin.Flag(
			"web.telemetry-path",
			"Path under which to expose metrics.",
		).Default("/metrics").String()
		serialPort = kingpin.Flag(
			"serial.port",
			"Serial port for the connection to the P1 interface.",
		).Required().String()
	)

	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	// Serial configuration
	config = &serial.Config{
		Name: *serialPort,
		Baud: 115200,
	}

	registry := prometheus.NewRegistry()

	registry.MustRegister(powerMeterActual)
	registry.MustRegister(powerMeterTotal)
	registry.MustRegister(instVoltage)
	registry.MustRegister(instCurrent)
	registry.MustRegister(gasMeterTotal)

	handler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})

	log.WithFields(log.Fields{
		"version": "unknown",
	}).Info("Starting P1 Exporter")

	recordMetrics()

	http.Handle(*metricsPath, handler)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(`<html>
							<head><title>P1 Exporter</title></head>
							<body>
							<h1>P1 Exporter</h1>
							<p><a href="` + *metricsPath + `">Metrics</a></p>
							</body>
							</html>`))

		if err != nil {
			log.Error(err)
		}
	})

	log.WithFields(log.Fields{
		"listen_address": *listenAddress,
		"metrics_path":   *metricsPath,
	}).Info("Listing on " + *listenAddress)

	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		log.Fatal(err)
	}
}
