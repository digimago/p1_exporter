module gitlab.com/digimago/p1_exporter

go 1.18

require (
	github.com/prometheus/client_golang v1.2.1
	github.com/roaldnefs/go-dsmr v0.0.0-20210207145743-27f646a51a10
	github.com/sirupsen/logrus v1.4.2
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190717042225-c3de453c63f4 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.0 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4 // indirect
	github.com/prometheus/common v0.7.0 // indirect
	github.com/prometheus/procfs v0.0.5 // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
)
